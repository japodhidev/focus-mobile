CREATE TABLE "users" (
  "id" SERIAL PRIMARY KEY NOT NULL,
  "full_name" character varying(255) NOT NULL,
  "email" character varying(255) NOT NULL,
  "created_at" timestamp
);

CREATE TABLE "prescription" (
  "id" SERIAL PRIMARY KEY,
  "code" character varying(255) NOT NULL,
  "details" character varying(255) NOT NULL,
  "expiration_date" date,
  "user_id" int
);

ALTER TABLE "prescription" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");
