[TOC]

# Focus Mobile Backend Take Home Challenge

My submission for the Focus Mobile backend challenge.

### CLI

#### Description

This program makes use of the resources available at the [RxNorm](https://rxnav.nlm.nih.gov/RxNormAPIs.html) API endpoint.
All responses will be printed on the terminal in `JSON` format.

The CLI interface is built using Python. I chose Python because it allows for fast prototyping. The program doesn't have
any external dependencies, all imports are inbuilt Python features. It requires `Python` version 3.x to run.
The typical python program invocation rules should therefore apply.

The program provides two command line argument options:

- `-s` - search option. This option should be followed by the prescription drug name to search for.
 An example is "Advil 200 mg Tab". [findRxcuiByString](https://rxnav.nlm.nih.gov/api-RxNorm.findRxcuiByString.html) is the function used in this case.
- `-d` - describe option. This option uses the provided argument to search for additional details from the 
[getNDCProperties](https://rxnav.nlm.nih.gov/api-RxNorm.getNDCProperties.html) function of the `RxNorm` API mentioned above.
 
> Note: The command line arguments should be provided enclosed within double quotes to ensure uniformity.
> There is no strict validation that enforces the use of double quotes since `Python` usually handles command line
> arguments with multiple spaces gracefully. The double quotes rule is more of a suggestion to prevent any issues that may arise
> should the end user's terminal mishandle arguments provided without proper escaping.

#### Usage
Here's an example of a program execution:

```sh
python main.py -s "Ibuprofen 200 mg tab"
# Sample response
# {'idGroup': {'name': 'Ibuprofen+200+mg+tab', 'rxnormId': ['310965']}}
python main.py -d 310965 # using the `rxnormId`
# {...}
```

### System Design Architecture

#### Description

The system architecture diagram was created using the tools available at [draw.io](https://www.diagrams.net/). All the relevant
documents are located inside the `architecture` directory of this repository. I have included a `png` image file of the final
diagram in case any problems arise while trying to open the `system.drawio` file.

A database dump (`hipaa.sql`) can be found within the `architecture` folder. `PostgreSQL (version 13)` is the database I used to create the table.

> Note: UUID's are available natively in PostgreSQL. The `uuid-ossp` module needs to be enabled by running the following command:
```
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";
```

### Survey Questions

The answers to the survey questions can be found in the file name `survey-answers.pdf`