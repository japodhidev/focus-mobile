import sys
import requests
import argparse

rxcui_endpoint = 'https://rxnav.nlm.nih.gov/REST/rxcui.json?'
ndcprops_endpoint = 'https://rxnav.nlm.nih.gov/REST/ndcproperties.json?'


def fetch(url, params):
    ''' Make a GET request to an endpoint. '''
    req = requests.get(url, params)

    if req.status_code == 200:
        print("Success. Found some data.")
        if req.json() is None:
            # fatal input error, exit program
            sys.exit("Something's wrong. Double check your input.")
        return req.json()
    else:
        # Exit gracefully
        print("Error code: " + req.status_code)
        sys.exit("Encountered a HTTP status code other than 200. Exiting.")


def parseSearchTerm(search_str):
    ''' Input sanitization checks to enforce uniformity. '''
    if type(search_str) is not str:
        print("Search term should be a string, {} provided instead".format(type(search_str)))
        sys.exit("Invalid input.")

    return search_str


def parseRxcuiParams(search_str):
    ''' Parse url (findRxcuiByString) params in preparation for the request. '''
    params = parseSearchTerm(search_str)
    if params:
        # Hacky url encoding, replace spaces with '+'
        concept_name = search_str.replace(' ', '+')
        search_type = 2

        return {'name': concept_name, 'search': search_type}
        pass
    pass


def parseNdcParams(search_str):
    ''' Parse url (getNDCProperties) params in preparation for the request. '''
    params = parseSearchTerm(search_str)
    if params:
        return {'id': params}
        pass


def main():
    ''' Program entrypoint '''
    # Command line argument setup
    parser = argparse.ArgumentParser(description="A small program to search the RxNorm data set.")
    parser.add_argument('-s', dest="search_term", type=str, help="Prescription drug name to search for e.g \"Advil 200 mg Tab\"")
    parser.add_argument('-d', dest="full_desc", type=str, help="NDC10 argument i.e RxCUI, NDC11, SPL_SET_ID")

    options = parser.parse_args()

    if options.search_term:
        params = parseRxcuiParams(options.search_term)
        print(fetch(rxcui_endpoint, params))
    elif options.full_desc:
        params = parseNdcParams(options.full_desc)
        print(fetch(ndcprops_endpoint, params))
        pass
    else:
        #no arguments provided, print help menu
        parser.print_help()
        pass
    pass


if __name__ == '__main__':
    main()
